<?php 
/**
 * @version $Id$
 * @copyright Center for History and New Media, 2007-2008
 * @license http://www.gnu.org/licenses/gpl-3.0.txt
 **/
 
/**
 * Thrown when the dropbox cannot import files
 * @author CHNM
 **/
class Dropbox_Exception extends Exception {}