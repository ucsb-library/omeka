<?php
define('HTML_PURIFIER_VERSION', get_plugin_ini('HtmlPurifier', 'version'));

// The directory in which to store the serialized cache files.
define('HTML_PURIFIER_CACHE_DIR', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'cache');

add_plugin_hook('install',     'html_purifier_install');
add_plugin_hook('initialize',  'html_purifier_initialize');
add_plugin_hook('config_form', 'html_purifier_config_form');
add_plugin_hook('config',      'html_purifier_config');

function html_purifier_install()
{   
    // Set all the default options.
    // List of allowed tags and HTML attributes.
    // 
    // If this list is set to empty, then all tags will be disallowed.
    set_option('html_purifier_allowed_admin', 'p,br,strong,em,span[style|class],div[style|class],ul,ol,li,a,h1,h2,h3,h4,h5,h6,address,pre,table,tr,td,blockquote,thead,tfoot,tbody,th');
    
    // Do not allow HTML through the public interface by default.
    set_option('html_purifier_allowed_public',    '');
        
    // Plugin should be enabled by default.
    set_option('html_purifier_purify_admin',      1);
    set_option('html_purifier_purify_public',     1);
}

function html_purifier_initialize()
{
    // Do not purify if the POST is empty.
    if (empty($_POST)) {
        return;
    }
    
    $allowedHtml = html_purifier_html_is_allowed();
    
    // If this returns null, this plugin should not run.
    // 
    // This needs to be here b/c the plugin can be selectively disabled for
    // admin and public themes.
    if ($allowedHtml === null) {
        return;
    }
    
    // Require the HTML Purfier autoloader.
    require_once 'htmlpurifier-3.1.1-lite/library/HTMLPurifier.auto.php';
    $htmlPurifierConfig = HTMLPurifier_Config::createDefault();
        
    // Allow HTML tags. Setting this as NULL allows a subest of TinyMCE's 
    // valid_elements whitelist. Setting this as an empty string disallows 
    // all HTML elements.
    // Storing this setting as a string in the database ensures that users have
    // full control over the HTML attributes that are allowed.
    $htmlPurifierConfig->set('HTML', 'Allowed', $allowedHtml);
    
    if (is_writable(HTML_PURIFIER_CACHE_DIR) and is_executable(HTML_PURIFIER_CACHE_DIR)) {
        // Set the caching dir.
        $htmlPurifierConfig->set('Cache', 'SerializerPath', HTML_PURIFIER_CACHE_DIR);
    } else {
        //Disable caching.
        $htmlPurifierConfig->set('Cache', 'DefinitionImpl', null);
    }
    
    
    // Get the purifier as a singleton.
    $htmlPurifier = HTMLPurifier::instance($htmlPurifierConfig);
            
    // Set this in the registry so that other plugins can get to it.
    Zend_Registry::set('html_purifier', $htmlPurifier);
    
    // Build a ZF controller plugin so that we can hijack specific requests to 
    // strip out POST data (if necessary).
    require_once 'HtmlPurifier_Controller_Plugin.php';
    $htmlPurifierPlugin = new HtmlPurifier_Controller_Plugin($htmlPurifier);
    Zend_Controller_Front::getInstance()->registerPlugin($htmlPurifierPlugin);
}

/**
 * Determine which HTML tags are allowed for data entry on the administrative 
 * interface.
 * 
 * @return string|null Returns null if plugin has not been enabled (either for
 * admin or public, depending).  String of allowed HTML elements and attributes 
 * otherwise.
 **/
function html_purifier_html_is_allowed()
{
    // Continue processing in the admin context.
     if (is_admin_theme()) {
         // Do not purify if the administrator opts out.
         if (!get_option('html_purifier_purify_admin')) {
             return;
         }
         $allowedHtmlElements    = get_option('html_purifier_allowed_admin');
     // Continue processing in the public context.
     } else {
         // Do not purify if the administrator opts out.
         if (!get_option('html_purifier_purify_public')) {
             return;
         }
         $allowedHtmlElements    = get_option('html_purifier_allowed_public');
     }
    return $allowedHtmlElements;    
}


function html_purifier_config_form()
{
    // Set the necessary options.
    $purifyAdmin      = get_option('html_purifier_purify_admin');
    $purifyPublic     = get_option('html_purifier_purify_public');
    $allowedAdmin     = get_option('html_purifier_allowed_admin');
    $allowedPublic    = get_option('html_purifier_allowed_public');
    
    // Include the configuration form.
    include 'config_form.php';
}

function html_purifier_config($post)
{    
    // Set all the user-defined options.
    set_option('html_purifier_allowed_admin',     trim($post['html_purifier_allowed_admin']));
    set_option('html_purifier_allowed_public',    trim($post['html_purifier_allowed_public']));
    set_option('html_purifier_purify_admin',      $post['html_purifier_purify_admin']);
    set_option('html_purifier_purify_public',     $post['html_purifier_purify_public']);
}
