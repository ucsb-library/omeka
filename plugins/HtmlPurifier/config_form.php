<p>&#8220;<a href="http://htmlpurifier.org/">HTML Purifier</a> is a standards-compliant 
HTML filter library written in PHP. HTML Purifier will not only remove all 
malicious code (better known as XSS) with a thoroughly audited, secure yet 
permissive whitelist, it will also make sure your documents are standards 
compliant, something only achievable with a comprehensive knowledge of W3C's 
specifications.&#8221;</p>

<?php if (!is_writable(HTML_PURIFIER_CACHE_DIR) or !is_executable(HTML_PURIFIER_CACHE_DIR)): ?>
    <p class="error">Please modify the permissions of the cache/ directory in
        this plugin.  The HtmlPurifier plugin needs read/write/execute access to
        this caching directory in order to avoid a major performance hit.</p>
<?php endif; ?>

<h3>Admin Configuration</h3>

<div class="field">
    <label for="html_purifier_purify_admin">"Purify" form data submitted through the admin interface?</label>
    <input type="hidden" name="html_purifier_purify_admin" value="0" />
    <div class="inputs"
><input type="checkbox" name="html_purifier_purify_admin" value="1" <?php if ($purifyAdmin) echo 'checked="checked" '; ?>/>
 <p class="explanation">Remove malicious code and reformat HTML to make it standards compliant. If this is not checked, form data will be processed normally.</p>
</div>
   
</div>

<div class="field">
    <label for="html_purifier_allowed_admin">Allowed HTML (admin forms):</label>
    <div class="inputs"><textarea name="html_purifier_allowed_admin" rows="8" cols="50"><?php echo $allowedAdmin; ?></textarea>
    <p class="explanation">Please enter any HTML elements allowed in administrative form posts, separated by commas. Do not include angle brackets, &lt; or &gt;. If this list is empty, no HTML will be allowed.</p>
</div>
</div>

<h3>Public Configuration</h3>

<div class="field">
    <label for="html_purifier_purify_public">"Purify" form data submitted through the public site?</label>
    <input type="hidden" name="html_purifier_purify_public" value="0" />
    <div class="inputs"><input type="checkbox" name="html_purifier_purify_public" value="1" <?php if ($purifyPublic) echo 'checked="checked" '; ?>/>
    <p class="explanation">Remove malicious code and reformat HTML to make it standards compliant. If this is not checked, form data will be processed normally.</p>
</div>
</div>

<div class="field">
    <label for="html_purifier_allowed_public">Allowed HTML (public forms):</label>
    <div class="inputs"><textarea name="html_purifier_allowed_public" rows="8" cols="50"><?php echo $allowedPublic; ?></textarea>
    <p class="explanation">Please enter any HTML elements allowed in public form posts, separated by commas. Do not include angle brackets, &lt; or &gt;. If this list is empty, no HTML will be allowed.</p>
</div>
</div>