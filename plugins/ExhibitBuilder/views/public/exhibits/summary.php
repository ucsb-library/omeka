<?php head(array('title' => html_escape('Summary of ' . exhibit('title')),'bodyid'=>'exhibit','bodyclass'=>'summary')); ?>
<div id="primary">
<h1><?php echo html_escape(exhibit('title')); ?></h1>
<?php echo exhibit_builder_section_nav(); ?>

<h2>Description</h2>
<?php echo exhibit('description'); ?>

<?php if($credits = exhibit('credits')): ?>
<h2>Credits</h2>
<p><?php echo html_escape($credits); ?></p>
<?php endif; ?>

</div>
<?php foot(); ?>