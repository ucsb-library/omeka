</div><!-- end content -->

<div id="footer">
<table border="0" summary="footer layout" id="footer-layout-table">
	<tr>
		<td>
		<p>Copyright &copy; <?=date('Y');?> <br />The Regents of the University of California, All Rights Reserved.</p>
		</td>
		<td rowspan="2">
			<p> 
			<a href="http://www.library.ucsb.edu/">Davidson Library</a> (805) 893-2478 
    	    <br  />
    	    <a href="http://www.library.ucsb.edu/arts/">Arts Library</a> (805) 893-2850 
			<br  />
			<a href="http://www.library.ucsb.edu/">UC Santa Barbara</a> Santa Barbara CA 93106 (805) 893-8000
			</p>
		</td>
	</tr>
    <tr>
    	<td colspan="1">
		<p>
		<a href="http://www.library.ucsb.edu/" title="UCSB Library Home">Library Home</a> 
		| <a href="http://www.ucsb.edu/" title="">UCSB Home</a> 
		| <a href="/contact" title="">Contact Us</a> 
		<!--<a href="http://www-stage.library.ucsb.edu/node/385" title="Accessibility">Accessibility</a> | -->
		</p>
		</td>
	</tr>
	</table>		
</div><!-- end footer -->
</div><!-- end wrap -->
<!-- Piwik --> 
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://analytics.library.ucsb.edu/" : "http://analytics.library.ucsb.edu/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 11);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://analytics.library.ucsb.edu/piwik.php?idsite=11" style="border:0" alt="" /></p></noscript>
<!-- End Piwik Tracking Tag -->

</body>
</html>